import os
from distutils.util import strtobool

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    APP_LOG_LEVEL = "DEBUG"
    HTTP_LOG_LEVEL = "DEBUG"
    LOG_COLOR = True
    RTSP_URL = os.environ.get("RTSP_URL", "http://rtsp.example.com")
    HOST = "localhost"
    PORT = "5000"
    SCHEME = "http"
    NERD_STATS = False
    SKIP_FRAME_COUNT = 15

    def __init__(self):
        """
        Initialize the Configuration to an instance
        """
        self.APP_LOG_LEVEL = os.environ.get("APP_LOG_LEVEL", "DEBUG").upper()
        self.HTTP_LOG_LEVEL = os.environ.get("HTTP_LOG_LEVEL", "DEBUG").upper()
        # Turns on/off the log color for output
        self.LOG_COLOR = strtobool(os.environ.get("LOG_COLOR", "true"))
        self.RTSP_URL = os.environ.get("RTSP_URL", "http://rtsp.example.com")
        self.HOST = "localhost"
        self.PORT = os.environ.get("SERVICE_PORT", "5000")
        self.SCHEME = "http"
        self.NERD_STATS = strtobool(os.environ.get("NERD_STATS", "false"))
        self.SKIP_FRAME_COUNT = os.environ.get("SKIP_FRAME_COUNT", 15)  # Default to 15. If Zero, then none
