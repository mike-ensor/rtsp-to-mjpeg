# Overview

Its 2024 and we still don't have an easy method for RTSP to Browser. 

## Approach

1. Use Python (Flask) + OpenCV to stream an RTSP to MotionJPEG
2. Use a `div` and `canvas` with `background-url` to display the streaming JPEG images from the RTSP feed
3. Deployable to Kubernetes

## Definitions

Source
: The URI of an RTSP source

Stream
: The name to use for the MJPEG endpoint

Endpoint
: The HTTP endpoint the SRC can refer to

## Config

### Building Locally

```shell
docker build -t rtsp-to-mjpeg .
```

### Running With Kubernetes

1. Set ENV variables in the `/manifest` 
   ```shell
   export APP_LOG_LEVEL="debug"                     # Optional, defaults to 'info'
    export HTTP_LOG_LEVEL="info"                    # Optional, defaults to 'info'
    export SERVICE_PORT="5000"                      # Optional, defaults to 5000
    export RTSP_URL="rtsp://< YOUR LINK HERE >"     # Required
    export LOG_COLOR="true"                         # Optional, defaults to true
   ```
   - Source the `.envrc` or environment variables before running application

1. Deploy to K8s

   ```shell
   kubectl apply -f manifests/rtsp-to-mjpeg.yaml
   ```

1. View Service or LoadBalancer

### Running Locally

1. Set ENV variables. Suggest using `direnv`
   ```shell
   export APP_LOG_LEVEL="debug"                     # Optional, defaults to 'info'
    export HTTP_LOG_LEVEL="info"                    # Optional, defaults to 'info'
    export SERVICE_PORT="5000"                      # Optional, defaults to 5000
    export RTSP_URL="rtsp://< YOUR LINK HERE >"     # Required
    export LOG_COLOR="true"                         # Optional, defaults to true
   ```
   - Source the `.envrc` or environment variables before running application

1. Run the application (Flask or Docker)
   - With Flask (Python 3.9.x required)
       ```shell
       pip install -r requiements.txt
       flask run --debug
       ```

   - With Docker (after previous step)
       ```shell 
       # Add any other ENV vars as desired
       docker run -it --env RTSP_URL=${RTSP_URL} -p 8080:5000 rtsp-to-mjpeg     
       ```

## Using on a page

The preferred method to use is to create a `canvas` surrounded by a `div` and to use the `background-image` to point 
at the `/stream` URL.  The second appraoch is to use a `canvas` over the top of an `img` which has the `src=/stream`.

Both examples are demostrated at `http://localhost:5000/` when the application is running

```css
.overlay-image {
    position: absolute;
    top: 0px; /* Relative to the `.image-container` div */
    left: 0px; /* Relative to the `.image-container` div */
    z-index: 100; /* Ensure overlay is on top */
}

/*  Preferred method */
.image-container {
    position: relative;
    width: 640px; /* Adjust as needed */
    height: 480px; /* Adjust as needed */
    background-image: url("/stream");
    background-position: left top; /* Positions for each image */
    background-size: 640px 480px; /* NOTE: This needs to match the size of the image desired */
    background-repeat: no-repeat, no-repeat;
    border: 1px #ffcc00;
}
```
```html
<div class="image-container">
    <canvas id="canvas" width="640" height="480" class="overlay-image"></canvas>
</div>
```

> NOTE: The stream needs to `/start` and `/stop` to be streaming images. The default `no-feed` is shown when there is no 
> feed or there is an issue.

## Series Links

This project is one component in a multi-component solution. The composed parts make up the game "Price a Tray".

https://gitlab.com/mike-ensor/price-a-tray-dynamic-frontend
: This is the User Experience and User Interface for the game

https://gitlab.com/mike-ensor/price-a-tray-backend
: This is the Game API where rules, game lifecycle, scores and data are stored

https://gitlab.com/mike-ensor/rtsp-to-mjpeg.git
: This project turns a RTSP stream into a MJPEG stream for consumption by the UI

https://gitlab.com/mike-ensor/price-a-tray-game-package
: This is the project used to deploy to a Kubernetes project with ConfigSync installed

https://gitlab.com/mike-ensor/price-a-tray-inference-server
: This project provides an API to inference extraced frames (image) against the edge model pulled from the GCS bucket.
