import errno
import logging
import multiprocessing
import threading
import time
from pickle import UnpicklingError

import cv2
from flask import current_app

config = current_app.config

# Convenience variables to make pipe read/write easier to read
FRAME_PRODUCING_PIPE = 0
FRAME_READING_PIPE = 1

log = logging.getLogger(current_app.name)
# Create a static global scope variable for faster access
COLLECT_FPS = config["NERD_STATS"]

threadLock = threading.Lock()


def log_nerd_stats(processed_frames):
    """ Log NERD stats to the console. Nerd stats are FPS observed over time """
    # If collecting & debug is enabled & frame_count % 30 == 1 (every 30 frames)
    return COLLECT_FPS == 1 and log.isEnabledFor(logging.DEBUG) and processed_frames % 30 == 1


class VideoCapture:
    # Accessed by all (thread and object)
    CAPTURE_STATE = False

    def __init__(self):
        self.target_url = None
        self.vcap = None
        self.t = None
        self.orig_stream_fps = None
        self.on_going_fps = None
        self.orig_width = None
        self.orig_height = None
        # Should log the nerd stats
        self.log_nerd_stats = config["NERD_STATS"]
        # After how many frames to skip 1
        self.frame_to_skip = config["SKIP_FRAME_COUNT"]
        self.is_ready = False
        self.source_url = None
        self.pipe = None
        self.video_type = None

    def init(self):
        self.target_url = None
        self.vcap = None
        self.t = None
        self.orig_stream_fps = None
        self.on_going_fps = None
        self.orig_width = None
        self.orig_height = None
        # Should log the nerd stats
        self.log_nerd_stats = config["NERD_STATS"]
        # After how many frames to skip 1
        self.frame_to_skip = config["SKIP_FRAME_COUNT"]
        self.is_ready = False
        self.source_url = None
        self.pipe = None
        self.video_type = None

    def set_video_type(self):
        """ Get the type of video being referenced """
        if self.target_url.startswith("file"):
            self.video_type = "demo"
        else:
            self.video_type = "live"

    def initialize(self, target_url, source_url):
        """
        Initializes the Video Feed
        :param target_url: The RTSP URL
        :param source_url: The Stream URL for the Image.Src
        :return:
        """
        self.source_url = source_url
        self.target_url = target_url
        self.pipe = multiprocessing.Pipe()  # Create a new pipe
        self.set_video_type()

        log.debug("Attempting to initialize video with {}".format(self.target_url))
        self.vcap = cv2.VideoCapture(self.target_url)  # will time out after 30 seconds, not configurable
        self.vcap.setExceptionMode(False)

        # Ensure video capture is opened
        if self.vcap.isOpened() is False:
            log.error("[Initialize Video Capture]: Error accessing RTSP stream. Check the exception log")
        else:
            self.is_ready = True
            # Setup Buffer sizing
            init_buffer = self.vcap.get(cv2.CAP_PROP_BUFFERSIZE)
            log.info("Initial Camera Buffer: {}".format(init_buffer))

            self.log_stream_info()

            # TODO: Any other video capture setup place here

            self.CAPTURE_STATE = True
            # Create Thread for Frame Capture
            self.t = threading.Thread(target=self.push_frame_thread, args=())

    def render_frame_from_pipe(self):
        """
        Fetches a frame from the pipe and yields to the output
        :return:
        """
        # TODO: What happens if the pipe is empty?
        while self.is_full_ready():
            try:
                frame = self.pipe[FRAME_READING_PIPE].recv()
            except (UnpicklingError, EOFError, cv2.error) as err:
                log.debug("No frames received from pipe. {}".format(err))
                continue

            # Convert frame to JPG format (likely already is, so maybe no-op in future?
            # Pipe producing would be where to look
            # if frame is not None:
            (_, frame_jpg) = cv2.imencode(".jpg", frame)

            out_frame = frame_jpg.tobytes()

            yield b"--frame\r\n" b"Content-type: image/jpeg\r\n\r\n" + out_frame + b"\r\n"

    def push_frame_thread(self):
        """
        Gets frames from the RTSP URL and pushes these to a Pipe
        :return:
        """

        if self.is_initialized() is not True:
            log.warning("[RTSP Pushing Frames]: RTSP stream is not ready")
        else:
            log.info("[RTSP Pushing Frames]: Starting RTSP Frame Pushing")

            total_time = 0
            processed_frames = 0
            self.on_going_fps = self.orig_stream_fps

            while self.is_stream_ready():

                start = time.time()
                try:
                    grabbed, frame = self.vcap.read()
                except (EOFError, cv2.error, UnpicklingError) as _:
                    # log.warning("Exception when grabbing frames. {}".format(err))
                    continue

                if grabbed is False:
                    if self.should_loop_video():
                        log.info("Restarting Demo Video")
                        self.vcap.set(cv2.CAP_PROP_POS_FRAMES, 0)
                    continue
                elif self.drop_to_catch_up(processed_frames, self.on_going_fps) is True:
                    processed_frames = processed_frames + 1
                    continue
                else:
                    try:
                        self.pipe[FRAME_PRODUCING_PIPE].send(frame)

                        end = time.time()

                        total_time += end - start
                        processed_frames = processed_frames + 1
                        self.on_going_fps = processed_frames / total_time

                        if log_nerd_stats(processed_frames):
                            # NOTE This will log FPS count times...don't run in production unless necessary
                            log.debug(
                                "Ongoing FPS: {:01.2f} - Stream FPS: {:01.2f}".format(self.on_going_fps,
                                                                                      self.orig_stream_fps))
                    except (IOError, cv2.error) as e:
                        log.error("Issue with sending to pipe: {}".format(e))
                        if e.errno == errno.EPIPE:
                            pass

            log.info("[RTSP Pushing Frames]: Closing RTSP stream")
            self.vcap.release()
            self.is_ready = False  # set the uninitialization # FIXME: Switch to Enums for states
            self.init()

    def should_loop_video(self):
        return "demo" == self.video_type

    def get_stream_log_info(self):
        output = ""
        if self.is_ready is True:
            output += "Stream type: {}".format(self.video_type)
            if self.should_loop_video():
                frame_count = self.vcap.get(cv2.CAP_PROP_FRAME_COUNT)
                output += "Demo MP4 Frane Count: {}".format(frame_count)
            self.orig_stream_fps = self.vcap.get(cv2.CAP_PROP_FPS)
            self.orig_width = self.vcap.get(cv2.CAP_PROP_FRAME_WIDTH)
            self.orig_height = self.vcap.get(cv2.CAP_PROP_FRAME_HEIGHT)
            output += "FPS: {}, Stream width: {}, height: {}".format(self.orig_stream_fps, self.orig_width,
                                                                     self.orig_height)

        return output

    def log_stream_info(self):
        stream_info = self.get_stream_log_info()

        log.info(stream_info)

        if self.orig_stream_fps > 20:
            log.warning("RTSP stream is detected to be {} above recommended 20FPS, stream may be laggy".format(
                self.orig_stream_fps))
        if self.orig_width > 1280 or self.orig_height > 720:
            log.warning("RTSP stream width-height({}x{}) is above recommended 1280x720, stream may be laggy".format(
                self.orig_width, self.orig_height))

    def start(self):
        self.CAPTURE_STATE = True
        self.t.start()  # Start the thread (now the pipe will start receiving frames)

    def status(self):
        return self.CAPTURE_STATE

    def current_fps(self):
        return self.on_going_fps

    def stop(self):
        self.CAPTURE_STATE = False
        # Need to wait till
        while self.is_initialized is True:
            log.debug("Closing stream")

        # self.vcap.release()
        self.vcap = None  # Full Reset (OpenCV object is no longer)

    def get_image_width(self):
        return self.orig_width

    def get_image_height(self):
        return self.orig_height

    def is_initialized(self):
        return self.is_ready is True

    def is_stream_ready(self) -> bool:
        return self.CAPTURE_STATE is True

    def is_full_ready(self):
        return self.is_initialized() and self.is_stream_ready()

    def drop_to_catch_up(self, processed_frames, on_going_fps):
        frame_to_skip = int(self.frame_to_skip)
        # No need to skip frames if we are at or over the target FPS
        if on_going_fps >= self.orig_stream_fps:
            return False
        if frame_to_skip is None or frame_to_skip < 0:
            return False
        else:
            return processed_frames % frame_to_skip == 1
