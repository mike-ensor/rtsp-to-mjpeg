from flask import Blueprint, current_app

logging = current_app.logger
config = current_app.config

bp = Blueprint("main", __name__)

from app.main import routes
