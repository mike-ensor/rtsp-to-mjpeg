from flask import (
    jsonify,
    render_template,
    send_file, Response, current_app, request, )
from flask_cors import cross_origin

from app.main import bp
from app.main.stream import VideoCapture

# Global instance of VideoCapture
VIDEO_FEED = VideoCapture()

log = current_app.logger
config = current_app.config


@bp.route("/favicon.ico")
def favicon():
    return send_file("static/favicon.ico", mimetype="image/vnd.microsoft.icon")


# Home page
@bp.route("/", methods=["GET"])
def index():
    log.info("HOME PAGE")
    return render_template("index.html")


@bp.route("/status", methods=["GET"])
def status_check():
    global VIDEO_FEED

    status = VIDEO_FEED.status()
    stats = ""  # Default to none
    fps = 0.0  # Default
    if status is True:
        stats = VIDEO_FEED.get_stream_log_info()
        fps = VIDEO_FEED.current_fps()

    return jsonify(
        {
            "app": "rtsp-to-mjpeg",
            "state": status,
            "stats": stats,
            "currentfps": fps,
        }
    )


@bp.route("/health", methods=["GET"])
def health_check():
    global VIDEO_FEED

    status = VIDEO_FEED.status()

    return jsonify(
        {"app": "rtsp-to-mjpeg",
         "state": status
         }
    )


@bp.route("/get-url", methods=["GET"])
@cross_origin()
def get_video_url():
    current_url = config["RTSP_URL"]
    log.debug("Current RTSP URL: {}".format(current_url))

    return jsonify(
        {
            "currentRTSPFeedURL": current_url
        }
    )


@bp.route("/set-url", methods=["GET"])
@cross_origin()
def set_video_url():
    current_url = config["RTSP_URL"]
    queued_url = request.args.get("url")
    # If there isn't a query parameter, do nothing, but respond back with current URL
    if queued_url is not None:
        config["RTSP_URL"] = queued_url
    else:
        queued_url = current_url

    log.debug("Current RTSP URL: {}".format(current_url))
    log.debug("Queued RTSP URL: {}".format(queued_url))

    return jsonify(
        {
            "currentRTSPFeedURL": current_url,
            "queuedRTSPFeedURL": queued_url
        }
    )


# TODO: Move these APIs to their own Blueprint
@bp.route("/start", methods=["GET"])
@cross_origin()
def start():
    global VIDEO_FEED

    if VIDEO_FEED.status() is True:
        return {"response": "Stream already running"}
    # this app's stream URL (not really sure if this is used, maybe remove later)
    stream_url = "{}://{}:{}/stream".format(config["SCHEME"], config["HOST"], config["PORT"])
    target_url = "{}".format(config["RTSP_URL"])
    # Attempt to initialize the RTSP feed
    log.info("Attempting to initialize the RTSP feed using RTSP URL: {}".format(target_url))

    # TODO: How to handle re-initialization
    VIDEO_FEED.initialize(target_url, stream_url)

    # Verify that the stream is ready, otherwise do not do anything
    if not VIDEO_FEED.is_initialized():
        log.warning("/start called, but Video Feed has not been initialized")
        response = jsonify({"stream_url": "none", "state": "uninitialized", "message": "Check the RTSP feed URL"})
    elif not VIDEO_FEED.is_stream_ready():
        log.debug("/stream called and stream HAS been initialized, but is NOT ready")
        response = jsonify({"stream_url": "none", "state": "initialized", "message": "RTSP feed is NOT READY"})
    else:
        log.info("Starting Stream from API /start")

        VIDEO_FEED.start()
        # TODO: Any way to verify this is true?

        width = VIDEO_FEED.get_image_width()
        height = VIDEO_FEED.get_image_height()

        response = jsonify({"stream_url": stream_url, "state": "running", "width": width, "height": height})
        log.debug("Starting Stream Response: {}".format(response))

    return response


@bp.route("/stop", methods=["GET"])
@cross_origin()
def stop():
    global VIDEO_FEED

    log.info("Stopping Stream")
    c = config
    url = "{}://{}:{}/stream".format(c["SCHEME"], c["HOST"], c["PORT"])
    response = jsonify({"stream_url": url, "state": "stopped"})
    log.debug("Stopping Stream Response: {}".format(response))

    VIDEO_FEED.stop()

    return response


@bp.route("/stream", methods=["GET"])
@cross_origin()
def stream():
    """
    Reads frames off the PIPE and yields them back to the caller
    :return:
    """
    global VIDEO_FEED

    if not VIDEO_FEED.is_initialized():
        log.warning("/stream called and stream has not been initialized, sending static no-feed.jpg")
        return send_file("static/no-feed.jpg", mimetype="image/jpeg")
    elif not VIDEO_FEED.is_stream_ready():
        log.debug("/stream called and stream HAS been initialized, but is NOT ready")
        return send_file("static/no-feed.jpg", mimetype="image/jpeg")
    else:
        return Response(
            # TODO: Is there going to be an issue if the pipe is empty?  What happens?
            VIDEO_FEED.render_frame_from_pipe(), mimetype="multipart/x-mixed-replace; boundary=frame"
        )
