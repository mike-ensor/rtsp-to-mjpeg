import logging
import sys

from colorlog import ColoredFormatter
from flask import Flask
from flask.logging import default_handler
from flask_cors import CORS

from config import Config


def create_app():
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object(Config())
    config = app.config

    # Set the logging for the http server
    werkzeug_log = logging.getLogger('werkzeug')
    werkzeug_log.setLevel(config["HTTP_LOG_LEVEL"])
    # Disable the web banner (kinda)
    cli = sys.modules['flask.cli']
    cli.show_server_banner = lambda *x: None

    show_color_logs = config["LOG_COLOR"]
    print("Color Logs: {}".format(show_color_logs))
    if show_color_logs:
        formatter = ColoredFormatter(
            '%(log_color)s%(levelname)-8s%(reset)s %(message)s',
            datefmt='%H:%M:%S',
            log_colors={
                'DEBUG': 'cyan',
                'INFO': 'green',
                'WARNING': 'yellow',
                'ERROR': 'red',
                'CRITICAL': 'red,bold'
            }
        )

        # Create a stream handler and set the formatter
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(formatter)

        # Add the stream handler to the app's logger
        app.logger.addHandler(stream_handler)
        app.logger.removeHandler(default_handler)

    app.logger.setLevel(config["APP_LOG_LEVEL"])

    app.app_context().push()

    # Initialize any other Flask extensions here

    # Register blueprints here
    from app.main import bp as main_bp
    app.register_blueprint(main_bp)
    # TODO: Create a second blueprint to separate out the WEB from the APIs

    # ensure the instance folder exists
    try:
        # os.makedirs(app.instance_path)
        app.logger.warning("Starting App")
    except OSError:
        pass

    app.logger.warning("APP_LOG_LEVEL: {}".format(config["APP_LOG_LEVEL"]))
    app.logger.warning("HTTP_LOG_LEVEL: {}".format(config["HTTP_LOG_LEVEL"]))
    app.logger.warning("LOG_COLOR: {}".format(show_color_logs))
    app.logger.warning("RTSP_URL: {}".format(config["RTSP_URL"]))
    nerd_stat_string = "True" if config["NERD_STATS"] == 1 else "False"
    app.logger.warning("COLLECT_FPS: {}".format(nerd_stat_string))
    app.logger.warning("SKIP_FRAME_COUNT: {}".format(config["SKIP_FRAME_COUNT"]))

    CORS(app)

    return app
