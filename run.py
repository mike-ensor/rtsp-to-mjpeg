#!/usr/bin/env python

from waitress import serve
from app import create_app
import os

if __name__ == "__main__":
    print("Starting server...")
    application = create_app()
    print("Application Created...")
    serve(application, host='0.0.0.0',
          port=int(os.environ.get('PORT', 5000)),
          threads=8,
          url_prefix='/'
          )
