from flask import Blueprint
import logging

from config import Config


logging.basicConfig(level=Config.LOG_LEVEL)

bp = Blueprint("main", __name__)

from app.main import routes
