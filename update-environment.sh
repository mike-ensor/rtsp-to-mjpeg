#!/bin/bash

pip-compile --generate-hashes requirements.in

if [[ $? -ne 0 ]]; then
    echo "pip-compile failed. Build failed"
    exit 1
fi

pip install -r requirements.txt
